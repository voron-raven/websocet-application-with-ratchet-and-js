$( document ).ready(function() {
  function wrap_message(msg, $element) {
    var html = '<div class="bubble bubble">' + msg + '</div>';

    html = $.trim(html);
   $element.html($element.html() + html);
  }

  var ws = new WebSocket('ws://localhost:8080');
  var $message = $('#message');

  ws.onopen = function(){
    $message.attr("class", 'label label-success');
    $message.text('open');
  };
  ws.onmessage = function(ev){
    $message.attr("class", 'label label-info');
    $message.hide();
    $message.fadeIn("slow");
    $message.text('recieved message');

    var json = JSON.parse(ev.data);

    console.log(json);
    wrap_message(json.message, $('.viewport'));

  };
  ws.onclose = function(ev){
    $message.attr("class", 'label label-important');
    $message.text('closed');
  };
  ws.onerror = function(ev){
    $message.attr("class", 'label label-warning');
    $message.text('error occurred');
  };

  $('.input-group-btn .btn-primary').click(function(event) {
    ws.send($(this).parent().parent().find('input').val());
    $(this).parent().parent().find('input').val('');
    return false;
  });
});